(in-package :netfarm)

(declaim (inline write-type-tag write-integer write-length)
         (optimize (speed 3)))
(defun write-type-tag (tag function)
  (declare ((unsigned-byte 8) tag)
           (optimize (speed 3) (safety 1))
           (function function))
  (let ((v (make-array 1 :element-type '(unsigned-byte 8))))
    (setf (aref v 0) tag)
    (funcall function v)
    t))

(defun write-integer (integer function)
  (declare (optimize (speed 3) (safety 1))
           (function function))
  (let ((byte-count (ceiling (integer-length integer) 8)))
    (assert (< byte-count 256))
    (write-type-tag byte-count function)
    (loop with buffer = (make-array byte-count
                                    :element-type '(unsigned-byte 8))
          for byte-position from (1- byte-count) downto 0
          for step = integer then (ash step -8)
          do (setf (aref buffer byte-position)
                   (logand step #xFF))
          finally (funcall function buffer))))

(defun write-length (length function)
  (declare (alexandria:array-length length)
           (optimize (speed 3) (safety 1))
           (function function))
  (let ((byte-count (ceiling (integer-length length) 8)))
    (assert (< byte-count 256))
    (write-type-tag byte-count function)
    (loop with buffer = (make-array byte-count
                                    :element-type '(unsigned-byte 8))
          for byte-position from (1- byte-count) downto 0
          for step of-type alexandria:array-length = length then (ash step -8)
          do (setf (aref buffer byte-position)
                   (logand step #xFF))
          finally (funcall function buffer))))

(defmacro string-to-octets* (string)
  #+sbcl
  `(sb-ext:string-to-octets ,string :external-format :utf-8)
  #-sbcl
  `(babel:string-to-octets ,string :encoding :utf-8))

(defun ascii-string-to-octets (string)
  "A STRING-TO-OCTETS for strings you know will only have ASCII characters, i.e. reference names."
  (declare (string string))
  (map '(vector (unsigned-byte 8)) #'char-code string))

(defvar *in-slot?* t)
(defun binary-render-to-function (object function)
  (declare (optimize (speed 3) (safety 1))
           (function function))
  (etypecase object
    (string
     (let ((bytes (string-to-octets* object)))
       (write-type-tag 1 function)
       (write-length (length bytes) function)
       (funcall function bytes)))
    (vector
     (write-type-tag 2 function)
     (write-length (length object) function)
     ;; Create a vector with element-type (UNSIGNED-BYTE 8)
     (if (equal (array-element-type object) '(unsigned-byte 8))
         (funcall function object)
         (funcall function (coerce object '(vector (unsigned-byte 8))))))
    (integer
     (write-type-tag (if (minusp object) 4 3)
                     function)
     (write-integer (abs object) function))
    (list
     (write-type-tag 5 function)
     (write-length (length object) function)
     (let ((*in-slot?* nil))
       (dolist (element object)
         (binary-render-to-function element function))))
    (reference
     (let ((bytes (ascii-string-to-octets (reference-hash object))))
       (write-type-tag 6 function)
       (write-length (length bytes) function)
       (funcall function bytes)))
    (object
     (let ((bytes (ascii-string-to-octets (hash-object* object))))
       (write-type-tag 6 function)
       (write-length (length bytes) function)
       (funcall function bytes)))
    (keyword
     (ecase object
       (:true (write-type-tag 7 function))
       (:false (write-type-tag 8 function))
       (:unbound
        (if *in-slot?*
            (write-type-tag 9 function)
            (error "Cannot write an unbound slot marker when not in a slot.")))))))

(defun write-hash-table (hash-table function)
  (let ((keys (sort (alexandria:hash-table-keys hash-table) #'string<)))
    (write-length (length keys) function)
    (dolist (key keys)
      (binary-render-to-function key function)
      (binary-render-to-function (gethash key hash-table) function))))

(defun write-metadata (object function)
  (write-length 1 function)
  (binary-render-to-function "schema" function)
  (binary-render-to-function (netfarm-class-name (class-of object)) function))

(defmacro write-from-vector (object function reader table)
  (alexandria:once-only (object function table)
    `(progn
       (write-integer (length ,table) ,function)
       (loop for name across ,table
             do (binary-render-to-function (,reader ,object name) ,function)))))

(defun write-vector (vector function)
  (write-length (length vector) function)
  (loop for value across vector
        do (binary-render-to-function value function)))

(defgeneric binary-render-object-to-function (object function
                                              emit-computed-values?
                                              emit-signatures?)
  (:method ((object object) function emit-computed-values? emit-signatures?)
    (when emit-signatures?
      (write-integer (length (object-signatures object)) function)
      (dolist (signature-pair (object-signatures object))
        (destructuring-bind (user . signature)
            signature-pair
          (funcall function (netfarm:hash-object user))
          (funcall function signature))))
    (let ((class (class-of object)))
      (write-metadata object function)
      (let ((*in-slot?* t))
        (write-from-vector object function
                           (lambda (object slot)
                             (slot-rendered-value object class slot))
                           (netfarm-class-rendered-slots class)))
      (when emit-computed-values?
        (let ((*in-slot?* t))
          (write-from-vector object function
                             (lambda (object slot)
                               (closer-mop:slot-value-using-class class object slot))
                             (netfarm-class-computed-slots class))))))
  (:method ((vague-object vague-object) function emit-computed-values? emit-signatures?)
    (when emit-signatures?
      (write-integer (length (vague-object-signatures vague-object))
                     function)
      (dolist (signature-pair (vague-object-signatures vague-object))
        (destructuring-bind (user . signature)
            signature-pair
          (funcall function (netfarm:hash-object user))
          (funcall function signature))))
    (write-hash-table (vague-object-metadata vague-object) function)
    (let ((*in-slot?* t))
      (write-vector (vague-object-values vague-object) function))
    (when emit-computed-values?
      (let ((*in-slot?* t))
        (write-vector (vague-object-computed-values vague-object) function)))))

(defun binary-render (object &optional function)
  (if (null function)
      (with-output-to-vector (f)
        (binary-render-to-function object #'f))
      (binary-render-to-function object function)))

(defun binary-render-object (object &key function
                                         (emit-computed-values t)
                                         (emit-signatures t))
  (if (null function)
      (with-output-to-vector (f)
        (binary-render-object-to-function object #'f
                                          emit-computed-values
                                          emit-signatures))
      (binary-render-object-to-function object function
                                        emit-computed-values
                                        emit-signatures)))
