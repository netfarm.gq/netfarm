(in-package :netfarm-scripts)

(defun %apply-side-effects (object effects)
  (loop for effect in effects
        do (apply #'%apply-side-effect object effect)))

(defvar *consistent-states* '())
(defun call-with-consistent-state (object continuation)
  ;; Allow for recursive calls to call-with-consistent-state
  (when (member object *consistent-states*)
    (return-from call-with-consistent-state (funcall continuation)))
  (let ((*consistent-states* (cons object *consistent-states*)))
    (bt:with-lock-held ((netfarm::consistent-state-effect-lock object))
      (setf (netfarm::consistent-state-state object) :consistent))
    (unwind-protect
         (bt:with-lock-held ((netfarm::consistent-state-lock object))
           (funcall continuation))
      (bt:with-lock-held ((netfarm::consistent-state-effect-lock object))
        (%apply-side-effects object (netfarm::consistent-state-effects object))
        (setf (netfarm::consistent-state-effects object) '())
        (setf (netfarm::consistent-state-state object) :free)))))

(defmacro with-consistent-state ((object) &body body)
  "Hold a consistent state of an object, deferring the application of any side effects that would be introduced from the outside world."
  `(call-with-consistent-state ,object (lambda () ,@body)))

(defun apply-side-effects-on (object side-effects)
  "Prepare to apply side effects on an object, and either apply them now, or when a consistent state has been released." 
  (bt:with-lock-held ((netfarm::consistent-state-effect-lock object))
    ;; Don't attempt to recursively lock or apply any other effects if
    ;; we have a consistent state of this object.
    (when (member object *consistent-states*)
      (%apply-side-effects object side-effects)
      (return-from apply-side-effects-on))
    ;; Otherwise, add these effects to the log.
    (alexandria:appendf (netfarm::consistent-state-effects object)
                        side-effects)
    ;; Try to apply side effects now, if no one is currently holding this
    ;; object.
    (when (eql (netfarm::consistent-state-state object) :free)
      (%apply-side-effects object (netfarm::consistent-state-effects object))
      (setf (netfarm::consistent-state-effects object) '()))))

(defun apply-side-effect (side-effect)
  (apply-side-effects-on (getf (rest side-effect) :target)
                         (list side-effect)))

(defun apply-side-effects (side-effects)
  (dolist (side-effect side-effects)
    (apply-side-effect side-effect)))
