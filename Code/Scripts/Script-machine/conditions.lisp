(in-package :netfarm-scripts)

(define-condition script-machine-signal () ()
  (:documentation "A signal created by the script machine when it wants to do something with the Netfarm universe.
The idea of this signal protocol is that some supervisor steps the script machine, and when it receives a signal,
it can either answer the signal or stash the machine's state if it cannot answer right away."))

(define-condition done (script-machine-signal) ())

(defmacro check-capability (capability-list capability-name operation-name)
  `(unless (member ',capability-name ,capability-list)
     (no-capability ',operation-name '(,capability-name) ',operation-name)))

(define-condition script-machine-error (error) ()
  (:documentation "An error signalled by the script machine when it tries to do something it is not allowed to do."))

(define-condition script-machine-signalled-error (error)
  ((message :initarg :message :reader script-machine-error-message)
   (cause   :initarg :cause   :reader script-machine-error-cause))
  (:report (lambda (c s)
             (format s "~s: ~a"
                     (script-machine-error-cause c)
                     (script-machine-error-message c)))))

(define-condition sent-procedure (script-machine-error)
  ((procedure :initarg :procedure :reader sent-procedure-procedure)
   (operation :initarg :operation :reader sent-procedure-operation))
  (:report (lambda (c s)
             (format s "The script machine tried to ~a with a value containing the procedure ~s"
                     (sent-procedure-operation c)
                     (sent-procedure-procedure c)))))

(define-condition program-counter-out-of-bounds (script-machine-error)
  ((program-counter :initarg :program-counter)))

(define-condition overflow (error)
  ((operation :initarg :operation :reader overflow-operation)
   (operands  :initarg :operands  :reader overflow-operands))
  (:report (lambda (c s)
             (format s "Overflow occured when evaluating (~a~{ ~a~})"
                     (overflow-operation c)
                     (overflow-operands c)))))

(define-condition no-capability (script-machine-error)
  ((operation :initarg :operation :reader no-capability-operation)
   (required-capabilities :initarg :required-capabilities
                          :reader no-capability-capabilities)
   (cause :initarg :cause :reader no-capability-cause))
  (:report (lambda (c s)
             (format s "The interpreter tried to use the instruction ~a,~:
                        which requires the capabilities ~{~a~^, ~}"
                     (no-capability-operation c)
                     (no-capability-capabilities c)))))

(defun no-capability (operation required-capabilities cause)
  (error 'no-capability
         :operation operation
         :required-capabilities required-capabilities
         :cause cause))

(define-condition invalid-instruction (script-machine-error)
  ((program-counter :initarg :program-counter)
   (instruction :initarg :instruction)))

(define-condition unrecoverable-error (script-machine-error)
  ())

(define-condition exceeded-cycle-limit (unrecoverable-error)
  ())

(define-condition method-missing (error)
  ((method :initarg :method)
   (object :initarg :object)))
