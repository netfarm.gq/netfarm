(in-package :netfarm-scripts)

;;; Manipulating Netfarm objects

(define-opcode 129 schema (() object) ()
  (if (typep object 'netfarm:object)
      (class->schema (class-of object))
      (error "~s does not have a schema" object)))

(defun check-for-procedures (datum operation)
  "Ensure there are no procedures in the given datum. If there are procedures, signal an error of type SENT-PROCEDURE with the first procedure found, and the given operation."
  (let ((work-list (list datum))
        (seen (make-hash-table :test 'eql)))
    (loop until (null work-list)
          do (let ((object (pop work-list)))
               (unless (gethash object seen) 
                 (typecase object
                   (cons (push (car object) work-list)
                    (push (cdr object) work-list))
                   (procedure (error 'sent-procedure
                                     :procedure object
                                     :operation operation)))
                 (setf (gethash object seen) t))))))

;;; (add-computed-value) name value --
(define-opcode* 130 add-computed-value ((:capabilities capabilities
                                         :side-effects side-effects
                                         :current-object object)
                                        name value)
    ()
  (check-type name string)
  (check-type object object)
  (check-for-procedures value 'add-computed-value)
  (check-capability capabilities :write-computed-values add-computed-value)
  (let ((slot (find-netfarm-computed-slot (class-of object) name)))
    (when (null slot)
      (error "~s does not have a computed slot named ~s"
             (class-of object)
             name))
    (push (list 'add-computed-value :target object
                                    :slot slot
                                    :value value)
          side-effects)))

;;; (remove-computed-value) name value --
(define-opcode* 140 remove-computed-value ((:capabilities capabilities
                                            :side-effects side-effects
                                            :current-object object)
                                           name value)
    ()
  (check-type name string)
  (check-type object object)
  (check-for-procedures value 'remove-computed-value)
  (check-capability capabilities :write-computed-values
                    remove-computed-value)
  (let ((slot (find-netfarm-computed-slot (class-of object) name)))
    (when (null slot)
      (error "~s does not have a computed slot named ~s"
             (class-of object)
             name))
    (push (list 'remove-computed-value :target object
                                       :slot slot
                                       :value value)
          side-effects)))

(define-opcode 131 object-value (() object slot-name) ()
  (let ((value (netfarm::object-rendered-value object slot-name)))
    (if (eql value :unbound)
        (error 'unbound-slot :instance object :name slot-name)
        value)))

;; (object-computed-values) object slot-name -- computed-values
(define-opcode 132 object-computed-values ((:capabilities capabilities)
                                           object slot-name)
    ()
  (check-capability capabilities :read-computed-values object-computed-values)
  (netfarm:object-computed-values object slot-name))

;; (object-boundp) object slot-name -- bound?
(define-function-opcode 133 (boolean-wrap object-boundp 2) 2
  :opcode-name object-boundp)

;; (object-authors) object -- authors
(define-opcode 134 object-authors (() object) ()
  (mapcar #'car (object-signatures object)))

;; Wait, why do we need this?
;; (object-name) object -- name
#+(or)
(define-opcode 136 object-name (() object) ()
  (hash-object* object))
