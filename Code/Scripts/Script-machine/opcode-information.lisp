(in-package :netfarm-scripts)

(declaim ((simple-array function (*)) *operators*))

(defun interpreter-lose (interpreter opcode)
  (error 'invalid-instruction
         :instruction opcode
         :program-counter (interpreter-program-counter interpreter)))

(defvar *operators*
  (make-array 256
	      :initial-element #'interpreter-lose
	      :element-type 'function))
(defvar *opcode-data* (make-array 256 :initial-element nil :element-type 'list))
(defvar *opcode-code* (make-array 256 :initial-element nil))

(defun opcode-information (opcode-number)
  (aref *opcode-data* opcode-number))

(defmacro with-interpreter-parts ((&key . #1=(program-counter variables program
                                              call-stack data-stack
                                              environment current-object last-object
                                              entry-points side-effects capabilities
                                              instruction-count))
                                  interpreter &body body)
  `(with-accessors*
       ((%%data-stack interpreter-data-stack)
        (%%program-counter interpreter-program-counter)
        (%%program interpreter-program)
        . ,(loop for variable in (list . #1#)
                 for accessor in (mapcar (lambda (symbol)
                                           (alexandria:format-symbol :netfarm-scripts
                                                                     "INTERPRETER-~A"
                                                                     symbol))
                                         '#1#)
                 unless (null variable)
                   collect (list variable accessor)))
       ,interpreter
     ,@body))

(defun make-opcode-lambda (parts inputs bytes declarations body)
  (let ((opcode!g (gensym "OPCODE"))
        (interpreter!g (gensym "INTERPRETER")))
    `(lambda (,interpreter!g ,opcode!g)
       (declare (interpreter ,interpreter!g)
                (ignore ,opcode!g))
       (with-interpreter-parts ,parts ,interpreter!g
         (let (,@(loop for input in (reverse inputs)
                       collect `(,input (pop* (interpreter-data-stack ,interpreter!g))))
               ,@(loop for byte in bytes
                       collect `(,byte (interpreter-read-byte ,interpreter!g))))
           (declare (ignorable ,@inputs ,@bytes))
           ,@declarations
           ,body)))))

(defmacro with-accessors* ((&rest accessor-bindings) subject &body body)
  "Basically WITH-ACCESSORS but you're allowed to use it for any object."
  (if (null accessor-bindings)
      `(progn ,subject ,@body)
      (alexandria:with-gensyms (subject-forced)
        `(let ((,subject-forced ,subject))
           (declare (ignorable ,subject-forced))
           (symbol-macrolet
               ,(loop for binding in accessor-bindings
                      collect (destructuring-bind (variable accessor) binding
                                `(,variable (,accessor ,subject-forced))))
;             (declare (ignorable . ,(mapcar #'first accessor-bindings)))
             ,@body)))))

(defmacro %define-opcode (number name interpreter inputs bytes declarations function body)
  `(setf *dispatch-function* nil
         (aref *opcode-data* ,number) '(,name ,inputs ,bytes)
         (aref *opcode-code* ,number) (list (list ',interpreter ',inputs ',bytes)
                                            ',declarations ',body)
         (aref *operators* ,number) ,function))

(defmacro define-opcode (number name (parts &rest inputs) (&rest bytes)
                         &body body &environment environment)
  "Define an opcode by creating a function that takes INPUTS variables from the INTERPRETER and appends the BODY's return values to the stack, and binding it to the NUMBERth operator in *OPERATORS*."
  (multiple-value-bind (body declarations)
      (alexandria:parse-body (loop for part in body
                                   collect (macroexpand part environment)))
    `(progn
       (let ((data (aref *opcode-data* ,number)))
         (unless (or (null data)
                     (eql ',name (first data)))
           (cerror "Redefine the opcode"
                   "The opcode ~d with name ~s is being redefined to ~s"
                   ,number (first data) ',name)))
       (%define-opcode ,number ,name ,parts ,inputs ,bytes
                       ,declarations
                       ,(make-opcode-lambda parts inputs bytes declarations
                                            `(push (progn ,@body) %%data-stack))
                       (push (progn ,@body) %%data-stack)))))

(defmacro define-opcode* (number name (parts &rest inputs) (&rest bytes)
                          &body body &environment environment)
  "Define an opcode by creating a function that takes INPUTS variables from the INTERPRETER pushing nothing to the stack, and binding it to the NUMBERth operator in *OPERATORS*."
  (multiple-value-bind (body declarations)
      (alexandria:parse-body (loop for part in body
                                   collect (macroexpand part environment)))
    `(progn
       (let ((data (aref *opcode-data* ,number)))
         (unless (or (null data)
                     (eql ',name (first data)))
           (cerror "Redefine the opcode"
                   "The opcode ~d with name ~s is being redefined to ~s"
                   ,number ',name (first data))))
       (%define-opcode ,number ,name ,parts ,inputs ,bytes
                       ,declarations
                       ,(make-opcode-lambda parts inputs bytes declarations `(block ,name ,@body))
                       (block ,name ,@body)))))

(defmacro define-function-opcode (number function argument-count &key (opcode-name function))
  (let ((arguments!g (loop repeat argument-count collect (gensym "ARGUMENT"))))
    (check-type opcode-name symbol)
    (if (symbolp function)
        `(define-opcode ,number ,opcode-name (() . ,arguments!g) ()
           (values (,function . ,arguments!g)))
        `(define-opcode ,number ,opcode-name (() . ,arguments!g) ()
           (values (funcall ,function . ,arguments!g))))))
