(in-package :netfarm-scripts)

(defvar *object-sorter*
  (lambda (list number-wanted query reverse?)
    (declare (ignore query reverse?))
    (subseq list 0 number-wanted))
  "A function of arguments LIST, NUMBER-WANTED, QUERY, REVERSE, which 
filters and/or sorts a list of objects using a client's recommender system.

This function is expected to return up to NUMBER-WANTED elements. 
(The recommender system may refuse to include very bad objects.)
QUERY is a query that the recommender should use, with +CURRENT-OBJECT-VARIABLE+ denoting the object that is being queried.
REVERSE is true if the results should be reported worst-to-best, or false if they should be reported best-to-worst.")

(defmacro with-object-sorter ((sorter) &body body)
  `(let ((*object-sorter* ,sorter))
     ,@body))

(defconstant +current-object-variable+ 'the-object)

(define-opcode 144 query-recommender ((:capabilities capabilities)
                                      objects number-wanted variable query reverse?)
    ()
  (check-capability capabilities :query-recommender query-recommender)
  (let* ((order (case reverse?
                 (:false nil)
                 (:true  t)
                 (otherwise (error "REVERSE? must be true or false, not ~s" reverse?))))
         (number-wanted (etypecase number-wanted
                          (integer (min (length objects) number-wanted))
                          (t (length objects))))
         (objects (funcall *object-sorter*
                           objects number-wanted
                           (substitute +current-object-variable+ variable
                                       query :test #'netfarm-equal)
                           order)))
    (check-type objects list)
    objects))
