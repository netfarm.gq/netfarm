(in-package :netfarm-scripts)

(let ((netfarm::*intern-hash-of-inbuilt-class?* nil))
  (defclass script ()
    ((entry-points :initarg :entry-points :reader script-entry-points)
     (methods :initform (make-hash-table :test 'equal)
              :netfarm-name nil
              :accessor script-method-table)
     (%methods :netfarm-name "methods"
               :initarg :methods
               :initform '()
               :reader script-methods)
     (program :netfarm-name nil :accessor %script-program-vector)
     (%program :netfarm-name "program" :initarg :program :reader script-program)
     (variables :initarg :variables :reader script-variables))
    (:metaclass netfarm::inbuilt-netfarm-class)
    (:schema-name "inbuilt@script")))

(defun setup-script-method-table (table methods)
  (dolist (method-entry-point methods)
    (destructuring-bind (name entry-point arguments)
        method-entry-point
      (setf (gethash name table)
            (list entry-point arguments)))))

(defun make-byte-vector (vector)
  (let ((byte-vector (make-array (length vector)
                                 :element-type '(unsigned-byte 8)
                                 :initial-element 0)))
    (replace byte-vector vector)
    byte-vector))

(defmethod initialize-instance :after ((script script) &key methods)
  (let ((table (script-method-table script)))
    (setf (%script-program-vector script)
          (make-byte-vector (script-program script)))
    (setup-script-method-table table methods)))

(defmethod fix-up-copied-object :after ((script script))
  (let ((table (make-hash-table :test 'equal))
        (methods (script-methods script)))
    (setf (%script-program-vector script)
          (make-byte-vector (script-program script)))
    (setf (script-method-table script) table)
    (setup-script-method-table table methods)))
