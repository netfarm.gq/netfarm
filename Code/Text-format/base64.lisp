(in-package :netfarm)

(defun base64->bytes (base64)
  (with-input-from-string (input base64)
    (decode-base64-bytes input)))

(defun bytes->base64 (bytes)
  (with-output-to-string (output)
    (encode-base64-bytes bytes output nil)))
