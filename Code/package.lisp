(defpackage :netfarm
  (:use :cl :ironclad :s-base64 :flexi-streams :split-sequence)
  (:export
   ;; formats
   #:binary-render #:binary-render-object
   #:parse-block #:render-object
   #:parse #:render
   #:binary-parse #:binary-parse-block
   #:apply-class #:fix-up-copied-object
   #:*reader-depth*
   #:base64->bytes #:bytes->base64
   ;; objects
   #:netfarm-class #:netfarm-class-scripts #:netfarm-class-message-script
   #:netfarm-class-presentation-script
   #:netfarm-class-name
   #:netfarm-class-slot-names #:netfarm-class-computed-slot-names
   #:netfarm-class-slot-position #:netfarm-class-computed-slot-position
   #:object-value #:object-makunbound #:object-boundp #:object-computed-values
   #:map-objects #:do-objects
   #:compute-dependencies #:deep-copy-object
   #:find-netfarm-class #:find-inbuilt-object
   #:netfarm-slot
   #:slot-netfarm-name #:netfarm-slot-position
   #:find-netfarm-slot #:find-netfarm-computed-slot
   #:map-slots #:map-computed-slots
   #:class->schema
   #:object-not-found #:object-not-found-name
   ;; schema
   #:schema #:schema-slots #:schema-scripts
   #:schema->class
   ;; types
   #:object #:vague-object #:reference
   ;; accessors
   #:vague-object-signatures #:vague-object-name
   #:vague-object-metadata #:vague-object-computed-values
   #:vague-object-source #:vague-object-values
   #:vague-object-schema-name
   #:map-references
   #:object-signatures #:object-source
   #:reference-hash
   ;; computed values
   #:add-computed-value #:remove-computed-value
   #:normalise-graph
   ;; inbuilt classes
   #:user #:schema
   #:user-sign-key #:user-ecdh-key #:user-data #:user-values
   #:mutable-mixin #:object-predecessor #:object-successors #:update-object
   #:user-map #:user-map-antitriples #:user-map-triples #:user-map-delegates
   #:mirror-class #:*mirror*
   ;; keys
   #:generate-keys #:make-keys
   #:keys-from-public #:shared-key
   #:keys-signature-public #:keys-signature-private
   #:keys-exchange-public #:keys-exchange-private
   #:symmetric-cipher
   #:symmetric-encrypt #:symmetric-decrypt
   #:keys
   ;; hashes
   #:hash-text #:sign-text
   #:hash-object #:hash-object*
   #:sign-object #:add-signature
   #:verify-text #:verify-object-hash
   #:verify-object-signatures #:verify-vague-object-signatures
   #:readable-verifier
   #:object->keys #:keys->object
   #:with-hash-cache #:with-lexical-hash-cache #:with-restored-lexical-hash-cache))
