(in-package :netfarm)

(defclass consistent-state-mixin ()
  ((lock        :initform (bt:make-lock)
                :accessor consistent-state-lock)
   (state       :initform :free
                :accessor consistent-state-state)
   (effect-lock :initform (bt:make-lock)
                :accessor consistent-state-effect-lock)
   (effects     :initform '()
                :accessor consistent-state-effects)))
