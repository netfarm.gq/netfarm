(in-package :netfarm)

;;; We _never_ want to intern this class.
(let ((*intern-class?* nil))
  (defclass object (rewriting-information-mixin consistent-state-mixin)
    ((signatures :initarg :signatures
                 :netfarm-name nil
                 :initform '() :accessor object-signatures))
    (:metaclass netfarm-class)))

;;; This appears to work better than an auxiliary method on
;;; class-direct-superclasses, or class-precedence-list.
;;; Now (typep (make-instance 'netfarm:schema) 'netfarm:object) ⇒ T,
;;; whereas modifying c-d-s would ⇒ NIL.
;;; Thanks to beach and phoe for weighing in on this in #lisp.
(defun patch-object-into-superclasses (superclasses)
  (if (some (lambda (class)
              (typep class 'netfarm-class))
            superclasses)
      superclasses
      ;; Patch in our OBJECT class.
      (let ((standard-object-class (find-class 'standard-object)))
        (append (remove standard-object-class superclasses)
                (list (find-class 'object) standard-object-class)))))

(defmethod initialize-instance :around ((class netfarm-class)
                                        &rest arguments &key name direct-superclasses)
  ;; Avoid trying to make OBJECT a superclass of itself.
  (if (eql name 'object)
      (call-next-method)
      (apply #'call-next-method class
             :direct-superclasses (patch-object-into-superclasses direct-superclasses)
             arguments)))

(defmethod reinitialize-instance :around ((class netfarm-class)
                                          &rest arguments
                                          &key (direct-superclasses '() ds-supplied?))
  (if (and ds-supplied? (not (eql (class-name class) 'object)))
      (apply #'call-next-method class
             :direct-superclasses (patch-object-into-superclasses direct-superclasses)
             arguments)
      (call-next-method)))
