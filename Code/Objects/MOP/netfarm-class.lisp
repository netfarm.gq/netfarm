(in-package :netfarm)

(defgeneric netfarm-class-scripts (class)
  (:method ((class standard-class)) '())
  (:method ((class closer-mop:forward-referenced-class))
    (cannot-accept-forward-referenced-class class)))

(defclass netfarm-class (standard-class)
  ((direct-scripts :initarg :scripts
                   :initform '()
                   :reader netfarm-class-direct-scripts
                   :accessor %netfarm-class-direct-scripts)
   (scripts :reader netfarm-class-scripts
            :accessor %netfarm-class-scripts)
   ;; A hash table of names -> slot definitions for OBJECT-VALUE,
   ;; and a vector of slots for rendering and parsing.
   (slot-table :reader netfarm-class-slot-table)
   (rendered-slots :reader netfarm-class-rendered-slots)
   (computed-slot-table :reader netfarm-class-computed-slot-table)
   (computed-slots :reader netfarm-class-computed-slots))
  (:documentation "The metaclass of a class Netfarm knows how to de/serialise, take OBJECT-VALUE of, create instances of, and get scripts from.
Providing the class option (:scripts script ...) will attach direct scripts to the class.
Each `script` is evaluated, but not in the lexical environment of the DEFCLASS form; because those are not evaluated by DEFCLASS, and we have to evaluate them with EVAL ourselves. "))

(defmethod closer-mop:validate-superclass ((class netfarm-class)
                                           (superclass standard-class))
  t)
(defmethod closer-mop:validate-superclass ((class standard-class)
                                           (superclass netfarm-class))
  t)

(defun force-script-data-evaluation (class)
  "Evaluate the provided forms that designate scripts."
  (setf (%netfarm-class-scripts class)
        (remove-duplicates
         (reduce #'append
                 (cons (mapcar #'eval (%netfarm-class-direct-scripts class))
                       (mapcar #'netfarm-class-scripts
                               (closer-mop:class-direct-superclasses class)))
                 :from-end t)
         :from-end t)))

(defvar *intern-class?* t)

(defmethod initialize-instance :after ((class netfarm-class) &key)
  (force-script-data-evaluation class)
  (closer-mop:finalize-inheritance class)
  (when *intern-class?*
    (intern-class class)))

(defmethod reinitialize-instance :after ((class netfarm-class) &key)
  (force-script-data-evaluation class)
  (when *intern-class?*
    (intern-class class)))

(defmethod netfarm-class-slot-table :before ((class netfarm-class))
  (unless (closer-mop:class-finalized-p class)
    (closer-mop:finalize-inheritance class)))
(defmethod netfarm-class-computed-slot-table :before ((class netfarm-class))
  (unless (closer-mop:class-finalized-p class)
    (closer-mop:finalize-inheritance class)))

(defgeneric netfarm-class-name (class)
  (:documentation "Pick the name that should be the name of the schema a vague object is described by.")
  ;; We also add a method when introducing INBUILT-NETFARM-CLASSes.
  (:method ((class netfarm-class))
    (hash-object* (class->schema class))))
 
(defgeneric name-vector (class names)
  ;; We also define a method for EXTERNAL-NETFARM-CLASSes.
  (:method ((class netfarm-class) names)
    "Sort the names in ASCII-betical (Unicode-betical? Pun doesn't work like that) order, in case there is somehow an inconsistency in how various Lisp implementations order slots." 
    (coerce (sort names #'string< :key #'slot-netfarm-name) '(vector string)))
  (:method :around ((class netfarm-class) names)
    (let ((vector (call-next-method)))
      (loop for slot across vector
            for position from 0
            do (setf (%slot-position slot) position))
      vector)))

(defun netfarm-class-slot-names (class)
  (map 'vector #'slot-netfarm-name (netfarm-class-rendered-slots class)))
(defun netfarm-class-computed-slot-names (class)
  (map 'vector #'slot-netfarm-name (netfarm-class-computed-slots class)))

(defmethod closer-mop:finalize-inheritance :after ((class netfarm-class))
  "Populate the slot and computed-slot tables."
  (let ((slot-table (make-string-hash-table))
        (computed-slot-table (make-string-hash-table))
        (slots (make-array 0 :adjustable t :fill-pointer 0))
        (computed-slots (make-array 0 :adjustable t :fill-pointer 0)))
    (labels ((maybe-add-slot (slot)
               (let ((name (slot-netfarm-name slot)))
                 (unless (null name)
                   (if (computed-slot-p slot)
                       (add-computed-slot slot name)
                       (add-slot slot name)))))
             (add-computed-slot (slot name)
               (when (string-gethash computed-slot-table name)
                 (error "duplicate computed slot name: ~s" name))
               (setf (string-gethash computed-slot-table name)
                     slot)
               (vector-push-extend slot computed-slots))
             (add-slot (slot name)
               (when (string-gethash slot-table name)
                 (error "duplicate slot name: ~s" name))
               (setf (string-gethash slot-table name)
                     slot)
               (vector-push-extend slot slots)))
      (dolist (slot (closer-mop:class-slots class))
        (when (typep slot 'netfarm-effective-slot)
          (maybe-add-slot slot)))
      (setf (slot-value class 'slot-table) slot-table
            (slot-value class 'rendered-slots) (name-vector class slots)
            (slot-value class 'computed-slot-table) computed-slot-table
            (slot-value class 'computed-slots) (name-vector class computed-slots)))))
