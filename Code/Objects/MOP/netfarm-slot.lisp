(in-package :netfarm)

;;; Direct slots
(defclass netfarm-slot (closer-mop:standard-direct-slot-definition)
  ((computed :initarg :computed
             :initform nil
             :reader computed-slot-p)
   (documentation :initarg :documentation
                  :reader slot-documentation)
   (renderer :initarg :renderer
             :initform nil
             :reader slot-renderer)
   (parser   :initarg :parser
             :initform nil
             :reader slot-parser)
   (netfarm-name :initarg :netfarm-name
                 :type (or null string)
                 :reader slot-netfarm-name)))
(defmethod print-object ((slot netfarm-slot) stream)
  (print-unreadable-object (slot stream :type t)
    (format stream "~s" (slot-netfarm-name slot))))

(defmethod initialize-instance :around ((slot netfarm-slot)
                                        &rest rest
                                        &key initform name computed)
  "Ensure that the initform of a computed slot is '()"
  (unless computed
    (return-from initialize-instance (call-next-method)))
  (unless (null initform)
    (error "Cannot supply an initform for the computed slot ~s"
           name))
  (apply #'call-next-method slot
         :initform ''()
         :initfunction (constantly '())
         rest))

(defmethod initialize-instance :after ((slot netfarm-slot) &key)
  ;; Generate a netfarm-slot name from the slot's symbol-name if one
  ;; wasn't already provided.
  (unless (slot-boundp slot 'netfarm-name)
    (setf (slot-value slot 'netfarm-name)
          (string-downcase (closer-mop:slot-definition-name slot)))))

(defmethod closer-mop:direct-slot-definition-class
    ((class netfarm-class) &rest initargs)
  (find-class 'netfarm-slot))

;;; Effective slots
(defclass netfarm-effective-slot (closer-mop:standard-effective-slot-definition)
  ((netfarm-name :initarg :netfarm-name
                 :initform nil
                 :accessor %slot-netfarm-name
                 :reader slot-netfarm-name)
   (computed     :initarg :computed
                 :initform nil
                 :accessor %slot-computed-p
                 :reader computed-slot-p)
   (renderer     :initarg :renderer
                 :accessor %slot-renderer
                 :reader slot-renderer)
   (parser       :initarg :parser
                 :accessor %slot-parser
                 :reader slot-parser)
   (position     :accessor %slot-position
                 :reader netfarm-slot-position)))

(defmethod initialize-instance :around ((slot netfarm-effective-slot)
                                        &rest arguments)
  "Trash the :type argument, because it can't be used with reference rewriting."
  (apply #'call-next-method slot
         :type t
         arguments))

(defvar *slot-is-inbuilt?* nil)
(defmethod closer-mop:effective-slot-definition-class ((class netfarm-class)
                                                       &key &allow-other-keys)
  (declare (ignore class))
  (if *slot-is-inbuilt?*
      (find-class 'closer-mop:standard-effective-slot-definition)
      (find-class 'netfarm-effective-slot)))
(defmethod closer-mop:compute-effective-slot-definition ((class netfarm-class)
                                                         name
                                                         slot-definitions)
  (let ((*slot-is-inbuilt?*
          (loop for slot in slot-definitions
                thereis (not (typep slot 'netfarm-slot))))
        (netfarm-slots (loop for slot in slot-definitions
                             when (typep slot 'netfarm-slot)
                               collect slot)))
    (when (null netfarm-slots)
      ;; Nothing to add then.
      (return-from closer-mop:compute-effective-slot-definition
        (call-next-method)))
    ;; Set up the Netfarm slot information: the name of the slot,
    ;; and if it is computed.
    (let ((first-computed (computed-slot-p (first netfarm-slots))))
      (assert (every (lambda (slot)
                       (eq first-computed (computed-slot-p slot)))
                     (rest netfarm-slots))
              ()
              "The slot ~s would be both computed and not computed."
              name)
      (let ((first-name (slot-netfarm-name (first netfarm-slots))))
        (assert (every (lambda (slot)
                         (equal first-name (slot-netfarm-name slot)))
                       (rest netfarm-slots))
                ()
                "The slot ~s would have multiple Netfarm names: ~{~a~^, ~}"
                name
                (mapcar #'slot-netfarm-name netfarm-slots))
        (let ((slotd (call-next-method)))
          (setf (%slot-netfarm-name slotd) first-name
                (%slot-computed-p slotd)   first-computed)
          ;; Pick the first renderer and parser to use.
          (setf (%slot-parser slotd)
                (or (some #'slot-parser slot-definitions) #'identity)
                (%slot-renderer slotd)
                (or (some #'slot-renderer slot-definitions) #'identity))
          slotd)))))

(declaim (inline find-netfarm-slot map-slots
                 find-netfarm-computed-slot map-computed-slots))
(defun find-netfarm-slot (class slot-name)
  "Find the effective slot definition for a Netfarm slot."
  (etypecase slot-name
    (symbol (setf slot-name (string-downcase slot-name)))
    (string))
  (string-gethash (netfarm-class-slot-table class) slot-name))

(defun map-slots (function class)
  "Call the function with each slot name and effective slot definition in the class."
  (let ((table (netfarm-class-slot-table class)))
    (loop for bucket across table
          do (loop for (name . definition) in bucket
                   do (funcall function name definition)))))

(defun find-netfarm-computed-slot (class slot-name)
  "Find the effective slot definition for a Netfarm computed slot."
  (etypecase slot-name
    (symbol (setf slot-name (string-downcase slot-name)))
    (string))
  (string-gethash (netfarm-class-computed-slot-table class) slot-name))

(defun map-computed-slots (function class)
  "Call the function with each computed slot name and definition in the class."
  (let ((table (netfarm-class-computed-slot-table class)))
    (loop for bucket across table
          do (loop for (name . definition) in bucket
                   do (funcall function name definition)))))

