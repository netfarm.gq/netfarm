(in-package :netfarm)

;;; Some inbuilt classes.
(eval-when (:compile-toplevel :load-toplevel :execute)
  (let ((*intern-class?* nil))
    (defclass schema ()
      ((inbuilt-name :netfarm-name nil
                     :initarg :inbuilt-name
                     :initform nil
                     :accessor %schema-inbuilt-name
                     :reader schema-inbuilt-name)
       (slots :initarg :slots
              :reader schema-slots
              :documentation "A list of slot definitions. Each slot definition is a list, starting with a string name for the slot.")
       (computed-slots :initarg :computed-slots
                       :reader schema-computed-slots
                       :documentation "A list of computed slot definitions.")
       (documentation :initarg :documentation
                      :reader schema-documentation
                      :documentation "A string describing this schema.")
       (scripts :initarg :scripts :initform '() :reader schema-scripts
                :documentation "A list of scripts that are run when an instance of this schema is admitted to a network. The first procedure in each script is called with the instance that has been admitted."))
      (:metaclass inbuilt-netfarm-class)
      (:schema-name "inbuilt@schema"))))

(defmethod print-object ((schema schema) stream)
  "If we can find a nice name for the class the schema uses, print that."
  (let ((hash (hash-object* schema)))
    (bt:with-lock-held (*hash-lock*)
      (let ((class (gethash hash *classes*)))
        (if (or (typep class 'external-netfarm-class)
                (null class))
            (call-next-method)
            (print-unreadable-object (schema stream :type t)
              (write (class-name class) :stream stream)))))))

(defmethod fix-up-copied-object ((schema schema))
  (unless (slot-boundp schema 'inbuilt-name)
    (setf (slot-value schema 'inbuilt-name) nil)))
