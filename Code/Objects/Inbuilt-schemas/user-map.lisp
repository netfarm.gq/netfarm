(in-package :netfarm)

(defclass user-map (mutable-mixin)
  ((antitriples :initarg :antitriples
                :initform '()
                :accessor user-map-antitriples)
   (triples :initarg :triples
            :initform '()
            :accessor user-map-triples)
   (delegates :initarg :delegates
              :initform '()
              :accessor user-map-delegates))
  (:metaclass inbuilt-netfarm-class)
  (:schema-name "inbuilt@map"))
