(in-package :netfarm)

(define-condition squirty-bottle (error)
  ((reason :initarg :reason :reader squirty-bottle-reason))
  (:report (lambda (c s)
             (format s "~&Bad user! You did something very, very bad:~%~2t~a"
                     (squirty-bottle-reason c)))))

(defun squirty-bottle (reason-control &rest reason-arguments)
  (error 'squirty-bottle
         :reason (apply #'format nil reason-control reason-arguments)))

(defun cannot-accept-forward-referenced-class (class)
  (error "Can't work with the forward referenced class ~s." class))

(define-condition object-not-found (error)
  ((name   :initarg :name :reader object-not-found-name)
   (source :initarg :source :reader object-not-found-source))
  (:report (lambda (c s)
             (format s "The object named ~a could not be found using ~s."
                     (object-not-found-name c)
                     (object-not-found-source c)))))
(defun object-not-found (name source)
  (error 'object-not-found :name name :source source))
