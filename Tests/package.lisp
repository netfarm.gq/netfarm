(defpackage :netfarm-tests
  (:use :cl :parachute :netfarm)
  (:export #:run-tests #:gitlab-ci-test))
