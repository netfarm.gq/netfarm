(in-package :netfarm-tests)

;;; A collection of things that should "round trip", i.e. produce the same
;;; value they were given.

(define-test round-trip
  :parent netfarm-tests
  (let* ((old-class (find-class 'netfarm:schema))
         (bytes (netfarm:binary-render-object
                 (netfarm:class->schema
                  (find-class 'netfarm:schema))))
         (new-class
           (netfarm:schema->class
            (netfarm:apply-class (netfarm:binary-parse-block bytes)
                                 (find-class 'netfarm:schema)))))
    (is eq old-class new-class
        "We should be able to render an inbuilt class, and parse it back to the same class.")))
